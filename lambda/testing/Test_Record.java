package lambda.testing;

import java.util.HashMap;
import java.util.Hashtable;

import lambda.Memory;
import lambda.term.App;
import lambda.term.Projection;
import lambda.term.Record;
import lambda.term.core.LTerm;
import lambda.type.Type;

public class Test_Record extends LTest {

	
	// (Lx:{x:NAT}.x.x){x=42, y=23} -> -> 42
	public void test03() {
		Memory m = MEM();

		Hashtable<String, LTerm> recordElements = new Hashtable<String, LTerm>();
		recordElements.put("x", NAT(42));
		recordElements.put("y", NAT(23));

		Hashtable<String, Type> t = new Hashtable<String, Type>();
		t.put("x", NAT());
		App app = APP(ABS("x", RECTYPE(t), PROJ(VAR("x"), "x")), REC(recordElements));

		assertEquals(app.reduce(m).reduce(m), NAT(42));
		assertEquals(app.type(ENV()), NAT());
	}	
	
	
	// (Lx:{x:NAT}.x.x){x=42} -> -> 42
	public void test02() {
		Memory m = MEM();

		Hashtable<String, LTerm> recordElements = new Hashtable<String, LTerm>();
		recordElements.put("x", NAT(42));

		Record r = REC(recordElements);

		Hashtable<String, Type> t = new Hashtable<String, Type>();
		t.put("x", NAT());
		
		App app = APP(ABS("x", RECTYPE(t), PROJ(VAR("x"), "x")), REC(recordElements));

		assertEquals(app.reduce(m). reduce(m), NAT(42));
	}	
	
	// {x=42}.x -> 42
	// {x=42}: {x:NAT}
	// {x=42}.x: NAT
	public void test01() {
		Memory m = MEM();

		HashMap<String, LTerm> recordElements = new HashMap<String, LTerm>();
		recordElements.put("x", NAT(42));

		HashMap<String, Type> t = new HashMap<String, Type>();
		t.put("x", NAT());
		assertEquals(REC(recordElements).type(ENV()), RECTYPE(t));

		
		Projection p = PROJ(REC(recordElements), "x");
		
		assertEquals(p.reduce(m), NAT(42));
		
		assertEquals(p.type(ENV()), NAT());
	}
	

	
	
}
