package lambda.testing;

public class Test_EqualsType extends LTest {
	
	public void testEquals() {
		assertEquals(NAT(), NAT());
		assertEquals(BOOL(), BOOL());
		assertTrue(!BOOL().equals(NAT()));

		assertEquals(FTYPE(NAT(), BOOL()), FTYPE(NAT(), BOOL()));
		assert(!(FTYPE(NAT(), BOOL()).equals(FTYPE(NAT(), NAT()))));
	}
}
