package lambda.testing;

import java.util.Map;

import junit.framework.TestCase;
import lambda.Environment;
import lambda.Memory;
import lambda.term.*;
import lambda.term.core.LTerm;
import lambda.type.BoolType;
import lambda.type.FunctionType;
import lambda.type.NatType;
import lambda.type.RecordType;
import lambda.type.SumType;
import lambda.type.Type;

public abstract class LTest extends TestCase {

	Var VAR(String name) {return new Var(name);}
	
	Type BOOL() {return new BoolType();}
	Type NAT() {return new NatType();}
	Memory MEM() {return new Memory();}
	 
	Type FTYPE(Type leftType, Type rightType) {return new FunctionType(leftType, rightType);}
	
	Environment ENV() {return new Environment();}
	Environment ENV(String varName, Type varType) {
		Environment e = new Environment();
		e.put(varName, varType);
		return e;
	}
	Environment ENV(String varName, Type varType, String v2, Type t2) {
		Environment e = ENV(varName, varType);
		e.put(v2, t2);
		return e;
	}

	
	Abs ABS(String v, Type t, LTerm body) {return new Abs(v, t, body);}
	App APP(LTerm left, LTerm right) {return new App(left, right);}
	Nat NAT(int value) {return new Nat(value);}
	Not NOT() {return new Not();}
	If IF(LTerm c, LTerm t, LTerm e) {return new If(c, t, e);}

	void THROWS_EXCEPTION(TestAction a) {
		try {
			a.action();
		} catch (Throwable ex) {
			return;
		}
		throw new RuntimeException("Should have thrown an exception");
	}

	public True TRUE() {
		return new True();
	}
	
	public False FALSE() {
		return new False();
	}

	public Case CASE(LTerm sumTerm, SumType type, String l, LTerm lt, String r, LTerm rt) {
		return new Case(sumTerm, type, l, lt, r, rt);
	}
	
	public SumType SUMTYPE(Type l, Type r) {
		return new SumType(l, r);
	}
	
	public Plus PLUS() {
		return new Plus();
	}

	public Record REC(Map<String, LTerm> recordElements) {
		return new Record(recordElements);
	}

	public Projection PROJ(LTerm term, String fieldName) {
		return new Projection(term, fieldName);
	}

	public RecordType RECTYPE(Map<String, Type> fieldTypes) {
		return new RecordType(fieldTypes);
	}

	public Cast CAST(LTerm term, Type t) { return new Cast(term, t); }

	public Let LET(String varName, LTerm term, LTerm target) { return new Let(varName, term, target); }

	public Instanceof IN(LTerm term, Type t) { return new Instanceof(term, t); }
	
	public Ref REF(LTerm term) { return new Ref(term); }
	public Assign ASSIGN(LTerm lValue, LTerm rValue) { return new Assign(lValue, rValue); }
}
