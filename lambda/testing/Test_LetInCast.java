package lambda.testing;


import lambda.Memory;
import lambda.term.core.LTerm;

public class Test_LetInCast extends LTest {
	public void test01() {
		Memory m = MEM();

		assertEquals(NAT(), CAST(NAT(42), NAT()).type(ENV())); // 42 AS Nat: Nat
		assertEquals(BOOL(), CAST(TRUE(), BOOL()).type(ENV())); // true AS Bool: Bool

		assertEquals(NAT(42), LET("x", NAT(42), VAR("x")).reduce(m));
		assertEquals(NAT(), LET("x", NAT(42), VAR("x")).type(ENV()));

		LTerm ret = IN(NAT(42), NAT()).reduce(m);

		assertEquals(TRUE(), IN(NAT(42), NAT()).reduce(m));
		assertEquals(BOOL(), IN(NAT(42), NAT()).type(ENV()));

		
	}
}
