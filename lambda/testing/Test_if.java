package lambda.testing;

import lambda.Memory;
import lambda.term.If;
import lambda.term.Inl;
import lambda.term.Inr;
import lambda.term.True;
import lambda.term.core.LTerm;
import lambda.type.BoolType;

public class Test_if extends LTest {

	
	public void test04() {
		LTerm anIf = APP(NOT(), 
					(IF(TRUE(), FALSE(), TRUE())));
		Memory m = MEM();
		assertTrue(anIf.type(ENV()) instanceof BoolType);
		assertTrue(anIf.reduce(m).reduce(m) instanceof True);

	}
	
	
	public void test03() {
		Memory m = MEM();
		LTerm anIf = APP(NOT(), 
				CASE(
					(IF(TRUE(), FALSE(), NAT(1))),
					SUMTYPE(BOOL(), NAT()),
					"x", VAR("x"),
					"y", TRUE()				
				));
		
		assertTrue(anIf.type(ENV()) instanceof BoolType);
		assertTrue(anIf.reduce(m).reduce(m).reduce(m) instanceof True);

	}

	public void test02() {
		Memory m = MEM();
		If anIf = IF(FALSE(), FALSE(), NAT(1));
		assertTrue(anIf.reduce(m) instanceof Inr);

	}
	public void test01() {
		Memory m = MEM();
		If anIf = IF(TRUE(), FALSE(), NAT(1));
		
		assertTrue(anIf.reduce(m) instanceof Inl);
		
	}
	
}
