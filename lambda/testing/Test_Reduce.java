package lambda.testing;

import lambda.Memory;
import lambda.term.Abs;
import lambda.term.App;
import lambda.term.False;
import lambda.term.True;
import lambda.term.core.LTerm;

public class Test_Reduce extends LTest {

	/*
	   (Lx. Ly. x) y  
	 */
	public void test04() {
		Memory m = MEM();

		LTerm t1 = ABS("x", BOOL(), ABS("y", BOOL(), VAR("x"))); 
		LTerm t2 = VAR("y");
		LTerm app = APP(t1, t2);
		LTerm red = app.reduce(m);
		assertTrue(!(red.freeVariables().isEmpty()));
	}
	
	/*
	   (Lx:Bool. NOT x) true -> NOT true -> false
	 */
	public void test03() {
		Memory m = MEM();

		LTerm t1 = ABS("x", BOOL(), APP(NOT(), VAR("x")));
		LTerm t2 = TRUE();
		LTerm app = APP(t1, t2);
		LTerm ret = app.reduce(m);
		assertTrue(ret instanceof App);
		assertTrue(((App) ret).left instanceof lambda.term.Not);
		assertTrue(((App) ret).right instanceof True);
		System.out.println(ret.isReducible());
		LTerm ret2 = ret.reduce(m);
		assertTrue(ret2 instanceof False);
		
	}

	
	/*
	   (Lx:Bool. x) (Lx:Bool. x) -> (Lx:Bool. x)
	 */
	public void test02() {
		Memory m = MEM();

		LTerm t1 = ABS("x", BOOL(), VAR("x"));
		LTerm t2 = ABS("x", BOOL(), VAR("x"));
		LTerm app = APP(t1, t2);
		LTerm ret = app.reduce(m);
		assertTrue(ret instanceof Abs);
	}

	/*
	   (Lx:Bool. x) true -> true
	 */
	public void test01() {
		Memory m = MEM();

		LTerm t = APP( ABS("x", BOOL(), VAR("x")) ,TRUE());
		LTerm l2 = t.reduce(m);
		assertTrue(l2 instanceof True);
	}
	
}
