package lambda.testing;

import lambda.Memory;
import lambda.term.App;
import lambda.term.Nat;
import lambda.term.Plus;
import lambda.term.core.LTerm;
import lambda.type.FunctionType;

public class Test_Plus extends LTest {
	
	public void testEquals() {
		Memory m = MEM();

		LTerm term = APP(APP(PLUS(), NAT(42)), NAT(1));
		assertTrue(((Nat) term.reduce(m).reduce(m)).value==43);

		term = APP(PLUS(), NAT(42));
		assertTrue((term.reduce(m).type(ENV())).equals(new FunctionType(NAT(), NAT())));
	
	
	}
}
