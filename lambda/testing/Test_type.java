package lambda.testing;

import lambda.term.core.LTerm;

public class Test_type extends LTest {
	

	public void test_type_simple() {
		
		assertEquals(NAT(), VAR("x").type(ENV("x", NAT()))); 	// {(x:Nat)} |= x:Nat
		THROWS_EXCEPTION(()->VAR("x").type(ENV()));				// {} |= x:Nat ist Fehler
		THROWS_EXCEPTION(()->VAR("x").type(ENV("y", NAT())));	// {(y:Nat)} |= x:Nat ist Fehler
		
		assertEquals(
				FTYPE(NAT(), NAT()), 
				ABS("x", NAT(), VAR("x")).type(ENV())); 		// {} |= Lx:Nat.x : Nat->Nat

		LTerm aTerm = ABS("x", NAT(), VAR("x")); // Lx:Nat.x
		assertTrue(
				aTerm.type(ENV()) != aTerm.type(ENV()));		// Types are NOT identical!

		assertEquals(
				NAT(),
				APP(ABS("x", NAT(), VAR("x")), NAT(1)).type(ENV()));// |= (Lx:Nat.x) 1 : Nat
	
	}



}
