package lambda;


import java.util.HashMap;

import lambda.type.Type;

public class Environment {

	public final HashMap<String, Type> map = new HashMap<>();
	
	public void put(String string, Type aType) {
		map.put(string, aType);
	}

	public boolean hasVar(String varName) {
		return map.containsKey(varName);
	}

	public Type getType(String varName) {
		return map.get(varName);
	}
	
	public Environment clone() {
		Environment e = new Environment();
		e.map.putAll(this.map);
		return e;
	}

	/*
	 * E,(x:t) = E 
	 */
	public Environment cloneWithVariable(String s, Type t) {
		Environment e = this.clone();
		e.put(s, t);
		return e;
	}
}
