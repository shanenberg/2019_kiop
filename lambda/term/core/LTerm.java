package lambda.term.core;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lambda.Environment;
import lambda.Memory;
import lambda.term.Record;
import lambda.type.Type;

public abstract class LTerm extends LTermConvenience {
	
	/*
	 * It returns the type of an expression. Every time a new (!) instance is returned.
	 */
	public abstract Type type(Environment e);
	
	/*
	 * It returns the reduced term. If a term is reduced, a new (!) term is created, i.e.
	 * a term does never ever change its state.
	 */
	public abstract LTerm reduce(Memory m);

	public abstract boolean isReducible();

	public abstract LTerm replaceFreeVar(String varName, LTerm insertedTerm);
	
	public boolean acceptsParameter() {
		return false;
	}

	public LTerm apply(LTerm right) {
		throw new RuntimeException("Cannot apply term");
	}

	public Set<String> freeVariables() {
		return new HashSet<String>();
	}

	public LTerm clone() {
		Field[] fields = this.getClass().getFields();
		ArrayList lTermParams = new ArrayList();
		Class[] paramTypes = new Class[fields.length];

		
		if (this.getClass()==Record.class) {
			System.out.println("lkajdsf");
		}
		
		try {
			
		
		int i = 0;
		for(Field field : fields) {
			paramTypes[i] = field.getType();
			
			if(field.getType().equals(LTerm.class)) {
				lTermParams.add(((LTerm) field.get(this)).clone());
			} else if (field.getType().equals(String.class)) {
				lTermParams.add(new String(((String) field.get(this))));
			} else if(field.getType().equals(Map.class)) {
				System.err.println("ljahfds");
				lTermParams.add(new HashMap(((Map) field.get(this))));
				System.out.println("dummy");
			} else {
				lTermParams.add(field.get(this)); // Nicht vergessen: Nun ein primiver, daher kein clone 		
			}
			i = i + 1;
		}
		
		Class<? extends LTerm> retClass = this.getClass();

			Constructor<? extends LTerm> constructor = retClass.getConstructor(paramTypes);
			return (LTerm) constructor.newInstance(lTermParams);
		} catch (Exception e1) {
			
			System.err.println("Klasse: " + this.getClass());
			
			
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		throw new RuntimeException("WTF");
	}
	
}
