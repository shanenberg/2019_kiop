package lambda.term.core;

import lambda.Environment;
import lambda.Memory;
import lambda.type.Type;

public abstract class LTermReducible extends LTerm {

    @Override
    public boolean isReducible() {
        return true;
    }
    
}
