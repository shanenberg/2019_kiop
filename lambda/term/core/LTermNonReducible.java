package lambda.term.core;

import lambda.Memory;

public abstract class LTermNonReducible extends LTerm {

	@Override
	public LTerm reduce(Memory m) {
		throw new RuntimeException("a " + this.getClass().getName() + " cannot be reduced");
	}

	@Override
	public boolean isReducible() {
		return false;
	}
}
