package lambda.term.core;

import java.util.HashSet;
import java.util.Set;

import lambda.Environment;
import lambda.Memory;
import lambda.type.Type;

public abstract class LTermConvenience {
	
	public LTerm reduceAll(Memory m) {
		LTerm current = (LTerm) this;
		while(current.isReducible()) {
			current = current.reduce(m);
		}
		return current;
	}
	
	
	
	public LTerm reduceAll() {
		Memory m = new Memory();
		return this.reduceAll(m);
	}

	public Type type() {
		return ((LTerm) this).type(new Environment());
	}
	
}
