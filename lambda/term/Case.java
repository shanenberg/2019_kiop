package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.In;
import lambda.term.core.LTerm;
import lambda.term.core.LTermReducible;
import lambda.type.SumType;
import lambda.type.Type;

public class Case extends LTermReducible {

	LTerm term;
	SumType sumType;
	
	String lTermVar;
	LTerm leftTerm;

	String rTermVar;
	LTerm rightTerm;
	
	@Override
	public Type type(Environment e) {
		Type type = term.type(e);

		if(!sumType.equals(type))
			throw new RuntimeException("wrong type");
		
		Environment e1 = e.cloneWithVariable(lTermVar, sumType.leftType);
		Type lType = leftTerm.type(e1);
		
		Environment e2 = e.cloneWithVariable(rTermVar, sumType.rightType);
		Type rType = rightTerm.type(e2);
		
		if(!lType.equals(rType))
			throw new RuntimeException("Alles kaputt");
		
		return lType;
	}
	
	@Override
	public LTerm reduce(Memory m) {
		if (term.isReducible())
			return new Case(term.reduce(m), sumType.clone(), lTermVar, leftTerm, rTermVar, rightTerm);
		else {
			In inTerm = (In) term;
			if (term instanceof Inl) {
				return leftTerm.replaceFreeVar(lTermVar, inTerm.term);
			} else {
				return rightTerm.replaceFreeVar(rTermVar, inTerm.term);
			}
		}
	}

	public Case(LTerm term, SumType sumType, String lTermVar, LTerm leftTerm, String rTermVar, LTerm rightTerm) {
		super();
		this.term = term;
		this.sumType = sumType;
		this.lTermVar = lTermVar;
		this.leftTerm = leftTerm;
		this.rTermVar = rTermVar;
		this.rightTerm = rightTerm;
	}

	
	@Override
	public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
		return new Case(
				term.replaceFreeVar(varName, insertedTerm),
				sumType.clone(),
				lTermVar,
				leftTerm.replaceFreeVar(varName, insertedTerm),
				rTermVar,
				rightTerm.replaceFreeVar(varName, insertedTerm));
	}
	
}
