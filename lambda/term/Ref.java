package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermReducible;
import lambda.type.RefType;
import lambda.type.Type;

public class Ref extends LTermReducible {
	
	final LTerm term;

	public Ref(LTerm term) {
		super();
		this.term = term;
	}

	@Override
	public Type type(Environment e) {
		return new RefType(term.type(e));
	}

	@Override
	public LTerm reduce(Memory m) {
		if (term.isReducible())
			return new Ref(term.reduce(m));
		m.memory.add(term.clone());
		return new Address(m.memory.size()-1); 
	}

	@Override
	public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
		return new Ref(term.replaceFreeVar(varName, insertedTerm));
	}

}
