package lambda.term;

import java.util.Set;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.type.FunctionType;
import lambda.type.Type;

public class App extends LTerm {

	final public LTerm left;
	final public LTerm right;
	
	
	public App(LTerm l, LTerm r) {
		left = l; right = r;
	}


	/*
		E |= t1: Tx->T     E |= t2: Tx
		==============================
		         E |= t1 t2: T
	*/
	public
	Type type(Environment e) {
		Type leftType = left.type(e);
		
		if (!(leftType instanceof FunctionType)) 
			throw new RuntimeException("Invalid Type: must be FunctionType");
		
		FunctionType tx_to_T = (FunctionType) leftType;
		Type rightType = right.type(e);
		
		if(!(rightType.isSubtypeOf(tx_to_T.left)))
			throw new RuntimeException("Applied term does not match Input Type - expected " + tx_to_T.left + " but got " + rightType);

		return tx_to_T.right;
	}


	/*
	 * t1 => t1'
	 * ===============
	 * t1 t2 => t1' t2
	 */
	public LTerm reduce(Memory m) {
		if(left.isReducible()) {
			LTerm newT = left.reduce(m);
			return new App(newT, right);
		} else if (right.isReducible()) {
			LTerm newT = right.reduce(m);
			return new App(left, newT);
		} else if (left.acceptsParameter()) {
			LTerm ret = left.apply(right);
			return ret;
		}
		throw new RuntimeException("Cannot reduce term");
	}


	@Override
	public boolean isReducible() {
		return left.isReducible() || right.isReducible() || left.acceptsParameter();
	}

	/**
	 * [x=t] t1 t2 = [x=t] t1 [x=t] t2;
	 */
	@Override
	public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
		return new App(
				left.replaceFreeVar(varName, insertedTerm), 
				right.replaceFreeVar(varName, insertedTerm));
	}
	
	/*
	 * FI(t1 t2) = FI(t1) UNION FI(t2)
	 */
	public Set<String> freeVariables() {
		Set<String> ret = left.freeVariables(); 
		ret.addAll(right.freeVariables());
		return ret;
	}
	

}
