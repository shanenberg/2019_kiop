package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermNonReducible;
import lambda.type.NatType;
import lambda.type.Type;

public class Nat extends LTermNonReducible {
	public final int value;

	public Nat(int value) {
		super();
		this.value = value;
	}

	@Override
	public Type type(Environment e) {
		return new NatType();
	}

	@Override
	public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
		return new Nat(value);
	}
	
	@Override
	public LTerm clone() {
		return new Nat(value);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nat other = (Nat) obj;
		if (value != other.value)
			return false;
		return true;
	}
	
	

}

