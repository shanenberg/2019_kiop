package lambda.term;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.type.RecordType;
import lambda.type.Type;

public class Record extends LTerm {

	public final Map<String, LTerm> fields;
	
	public Record(Map<String, LTerm> fields) {
		super();
		this.fields = fields;
	}

	@Override
	public Type type(Environment e) {
		Map<String, Type> fieldTypes = new HashMap<String, Type>();
		for( Entry<String, LTerm> aField: fields.entrySet())
				fieldTypes.put(aField.getKey(), aField.getValue().type(e));
		return new RecordType(fieldTypes);
	}

	@Override
	public LTerm reduce(Memory m) {
		Map<String, LTerm> newFields = new HashMap<String, LTerm>(fields);
		
		for(Entry<String, LTerm> aField: fields.entrySet()) {
			if (aField.getValue().isReducible())
				newFields.put(aField.getKey(), aField.getValue().reduce(m));	
		}
		
		return new Record(newFields);
	}

	@Override
	public boolean isReducible() {
		for(Entry<String, LTerm> aField: fields.entrySet()) {
			if (aField.getValue().isReducible())
				return true;
		}
		return false;
	}

	@Override
	public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
		Map<String, LTerm> newFields = new HashMap<String, LTerm>(fields);

		for(Entry<String, LTerm> aField: fields.entrySet()) {
				newFields.put(aField.getKey(), aField.getValue().replaceFreeVar(varName, insertedTerm));	
		}
		
		return new Record(newFields);
	}

}
