package lambda.term;

import java.util.Set;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermNonReducible;
import lambda.type.FunctionType;
import lambda.type.Type;

public class Abs extends LTermNonReducible {

	final String varName;
	final Type type;
	final LTerm body;
	
	public Abs(String v, Type t, LTerm b) {
		this.varName = v; this.type = t; this.body = b;
	}


	/*
		E, (x:T) |= t: T'
		===================
		E |= Lx:T. t: T->T'
	 */
	public Type type(Environment e) {
		Environment e1 = e.cloneWithVariable(varName,  type);
		Type retType = body.type(e1); 
		return new FunctionType(type, retType);
	}


	/*
	 * [head = insertedTerm] body
	 */
	public LTerm apply(LTerm insertedTerm) {
		Set<String> fv = insertedTerm.freeVariables();
		if (fv.contains(this.varName)) {
			Abs alphConverted = this.doAlphaConversionMatching(insertedTerm);
			return alphConverted.apply(insertedTerm);
		}
		
		LTerm ret = body.replaceFreeVar(varName, insertedTerm);
		return ret;
	}


	private Abs doAlphaConversionMatching(LTerm insertedTerm) {
		Set<String> fv = insertedTerm.freeVariables();

		String newName = this.varName;
		
		while(fv.contains(newName)) {
			newName = newName + "1";
		}
		
		LTerm alphaConvertedBody = body.replaceFreeVar(this.varName, new Var(newName));
		return new Abs(newName, this.type.clone(), alphaConvertedBody);
	}


	/*
	 * [y = t] Lx.t2 = Lx.[y=t] t2......wenn x!=x
	 */
	@Override
	public LTerm replaceFreeVar(String varToReplace, LTerm insertedTerm) {
		Abs alphaConvertedTerm = doAlphaConversionMatching(insertedTerm);

		if(!(varToReplace.equals(alphaConvertedTerm.varName))) {
			return new 
				Abs(alphaConvertedTerm.varName, 
					alphaConvertedTerm.type.clone(), 
					alphaConvertedTerm.body.replaceFreeVar(varToReplace, insertedTerm));
		}
		
		
		return alphaConvertedTerm.clone();
	}
	
	public boolean acceptsParameter() {
		return true;
	}

	/*
	 * FI(Lx.t) = FI(t) - {x}
	 */
	public Set<String> freeVariables() {
		Set<String> ret = body.freeVariables(); 
		ret.remove(varName);
		return ret;
	}
}
