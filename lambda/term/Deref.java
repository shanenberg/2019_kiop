package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermReducible;
import lambda.type.RefType;
import lambda.type.Type;

public class Deref extends LTermReducible {

	final LTerm term;
	
	public Deref(LTerm t) {
		super();
		this.term = t;
	}

	@Override
	public Type type(Environment e) {
		RefType r = (RefType) term.type(e);
		return r.elementType.clone();
	}

	@Override
	public LTerm reduce(Memory m) {
		if (term.isReducible())
			return term.reduce(m);
		return m.memory.get(((Address) term).key);
	}

	@Override
	public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
		return new Deref(term.replaceFreeVar(varName, insertedTerm));
	}


}
