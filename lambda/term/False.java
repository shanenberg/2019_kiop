package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermNonReducible;
import lambda.type.BoolType;
import lambda.type.Type;

public class False extends LTermNonReducible {

	@Override
	public Type type(Environment e) {
		return new BoolType();
	}

	@Override
	public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
		return new False();
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof False;
	}
}
