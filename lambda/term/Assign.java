package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermReducible;
import lambda.type.Type;
import lambda.type.UnitType;

public class Assign extends LTermReducible {
	public final LTerm lValue;
	public final LTerm rValue;
	
	public Assign(LTerm lValue, LTerm rValue) {
		super();
		this.lValue = lValue;
		this.rValue = rValue;
	}
	
	@Override
	public Type type(Environment e) {
		return new UnitType();
	}
	
	@Override
	public LTerm reduce(Memory m) {
		if (lValue.isReducible())
			return new Assign(lValue.reduce(m), rValue.clone());
		if (rValue.isReducible())
			return new Assign(lValue.clone(), rValue.reduce(m));
		return m.memory.set(((Address) lValue).key, rValue.clone());
	}
	

	@Override
	public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
		return new Assign(
				lValue.replaceFreeVar(varName, insertedTerm), 
				rValue.replaceFreeVar(varName, insertedTerm));
	}
	
	
}
