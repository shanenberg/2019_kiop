package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermReducible;
import lambda.type.BoolType;
import lambda.type.SumType;
import lambda.type.Type;

public class If extends LTermReducible {

	LTerm condition;
	LTerm thenTerm;
	LTerm elseTerm;

	public If(LTerm condition, LTerm thenTerm, LTerm elseTerm) {
		super();
		this.condition = condition;
		this.thenTerm = thenTerm;
		this.elseTerm = elseTerm;
	}
	
	@Override
	public Type type(Environment e) {

		if(!(condition.type(e) instanceof BoolType))
			throw new RuntimeException("condition in if must be boolean");
		
		Type thenType = thenTerm.type(e);
		Type elseType = elseTerm.type(e);
		
		if (thenType.equals(elseType))
			return thenType;
		else 
			return new SumType(thenType, elseType);
	}

	@Override
	public LTerm reduce(Memory m) {
		
		Type thenType = thenTerm.type(new Environment());
		Type elseType = elseTerm.type(new Environment());
		
		
		if(condition.isReducible()) {
			return new If(condition.reduce(m), thenTerm.clone(), elseTerm.clone());
		} else {
			if(condition instanceof True) {
				return new Inl(new SumType(thenType, elseType), thenTerm.clone());
			} else {
				return new Inr(new SumType(thenType, elseType), elseTerm.clone());
			}
		}
	}

	@Override
	public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
		return new If(
				condition.replaceFreeVar(varName, insertedTerm),
				thenTerm.replaceFreeVar(varName, insertedTerm),
				elseTerm.replaceFreeVar(varName, insertedTerm));
	}
	
}
