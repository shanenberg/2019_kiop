package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermNonReducible;
import lambda.type.BoolType;
import lambda.type.FunctionType;
import lambda.type.Type;

public class Not extends LTermNonReducible {

	@Override
	public Type type(Environment e) {
		return new FunctionType(new BoolType(), new BoolType());
	}

	@Override
	public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
		return this.clone();
	}

	public boolean acceptsParameter() {
		return true;
	}
	
	public LTerm apply(LTerm right) {
		if (right instanceof True)
			return new False();
		else
			return new True();
	}

}
