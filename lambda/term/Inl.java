package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.In;
import lambda.term.core.LTerm;
import lambda.type.SumType;
import lambda.type.Type;

public class Inl extends In {

	public Inl(SumType sumType, LTerm term) {
		super();
		this.sumType = sumType;
		this.term = term;
	}

	@Override
	public Type type(Environment e) {
		Type t = term.type(e);
		if (!sumType.leftType.equals(t))
			throw new RuntimeException("Inl requires appropriate type on left side");
		return sumType.clone();
	}

	@Override
	public LTerm reduce(Memory m) {
		LTerm tRet = term.reduce(m);
		return new Inl(sumType.clone(), tRet);
	}

	@Override
	public boolean isReducible() {
		return term.isReducible();
	}

	@Override
	public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
		return new Inl(sumType.clone(), term.replaceFreeVar(varName, insertedTerm));
	}


}
