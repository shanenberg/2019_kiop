package lambda.term;

import java.util.Set;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermNonReducible;
import lambda.type.Type;

public class Var extends LTermNonReducible {

	String varName;
	
	public Var(String name) {
		varName = name;
	}

	@Override
	/*         x:T � E
	 * t-Var ==============
	 *         E |= x:T
	 */
	public Type type(Environment e) {
		if (e.hasVar(varName))
			return e.getType(varName);
		else
			throw new RuntimeException("Stupid!");
	}

	/**
	 * [x1 := t] x2 = t (wenn x1=x2)
	 */
	@Override
	public LTerm replaceFreeVar(String varToReplace, LTerm insertedTerm) {
		if(varToReplace.equals(this.varName)) {
			return insertedTerm.clone();
		}
		return new Var(varName);
	}

	public Set<String> freeVariables() {
		Set<String> ret = super.freeVariables();
		ret.add(this.varName);
		return ret;
	}


}
