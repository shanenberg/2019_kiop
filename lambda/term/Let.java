package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermReducible;
import lambda.type.Type;

public class Let extends LTermReducible {
    String varName;
    LTerm term, target;

    public Let(String varName, LTerm term, LTerm target) {
        this.varName = varName;
        this.term = term;
        this.target = target;
    }

    @Override
    public Type type(Environment e) {
        if(term.type(e) instanceof Type) {
            Environment newE = e.cloneWithVariable(varName, term.type(e));
            return target.type(newE);
        }
        throw new RuntimeException("term does not have a type");
    }

    @Override
    public LTerm reduce(Memory m) {
        if(term.isReducible())
            return new Let(varName, term.reduce(m), target);
        return target.replaceFreeVar(varName, term);
    }

    @Override
    public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
        // TODO: Nachdenken: Behandeln wie die entsprechende Applikation?
        // Aber linke Seite ist eigentlich l this.varname . term
        if(this.varName.equals(varName)) {
            return new Let(this.varName,
                    term.replaceFreeVar(varName, insertedTerm),
                    target.clone());
        }
        return new Let(this.varName, term.replaceFreeVar(varName, insertedTerm), target.replaceFreeVar(varName, insertedTerm));
    }

}
