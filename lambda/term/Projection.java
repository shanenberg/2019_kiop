package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermReducible;
import lambda.type.RecordType;
import lambda.type.Type;

public class Projection extends LTermReducible {

	LTerm term;
	String fieldName;
	
	public Projection(LTerm term, String fieldName) {
		this.term = term;
		this.fieldName = fieldName;
	}

	@Override
	public Type type(Environment e) {
		return ((RecordType) (term.type(e))).fieldTypes.get(fieldName);
	}

	@Override
	public LTerm reduce(Memory m) {
		if (term.isReducible())
			return new Projection(term.reduce(m), fieldName);
		return ((Record) term).fields.get(fieldName).clone();
	}

	@Override
	public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
		return new Projection(term.replaceFreeVar(varName, insertedTerm), fieldName);
	}

}
