package lambda.term.helpers;

public interface ReducibleExpression {
	
	public default boolean isReducible() { return true; }
	
}
