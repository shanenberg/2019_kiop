package lambda.term.helpers;

import lambda.Memory;
import lambda.term.core.LTerm;

public interface NonReducibleExpression {
	
	public default boolean isReducible() { return false; }


	public default LTerm reduce(Memory m) {
		throw new RuntimeException("Expression " + this.getClass().getName() + " cannot be reduced.");
	}

	
}
