package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermReducible;
import lambda.type.BoolType;
import lambda.type.Type;

public class Instanceof extends LTermReducible {
    LTerm term;
    Type t;

    public Instanceof(LTerm term, Type t) {
        this.term = term;
        this.t = t;
    }

    @Override
    public Type type(Environment e) {
        if(term.type(e) instanceof Type)
            return new BoolType();
        throw new RuntimeException("term does not have a type");
    }

    @Override
    public LTerm reduce(Memory m) {
        if(term.isReducible())
            return new Instanceof(term.reduce(m), t);
        if(term.type(new Environment()).isSubtypeOf(t))
            return new True();
        return new False();
    }

    @Override
    public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
        return new Instanceof(term.replaceFreeVar(varName, insertedTerm), t);
    }

}
