package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermNonReducible;
import lambda.type.FunctionType;
import lambda.type.NatType;
import lambda.type.Type;

public class PlusX extends LTermNonReducible {

	public final Nat operand;
	
	public PlusX(Nat operand) {
		super();
		this.operand = operand;
	}

	@Override
	public Type type(Environment e) {
		return new FunctionType(new NatType(), new NatType());
	}

	@Override
	public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
		return new PlusX((Nat) (operand.clone()));
	}

	@Override
	public boolean acceptsParameter() {
		return true;
	}

	@Override
	public LTerm apply(LTerm right) {
		return new Nat(operand.value + ((Nat) right).value);
	}

}
