package lambda.term;

import lambda.Environment;
import lambda.Memory;
import lambda.term.core.LTerm;
import lambda.term.core.LTermReducible;
import lambda.type.Type;

public class Cast extends LTermReducible {
    LTerm term;
    Type t;

    public Cast(LTerm term, Type t) {
        this.term = term;
        this.t = t;
    }

    @Override
    public Type type(Environment e) {
        if(term.type(e) instanceof Type)
            return t.clone();
        throw new RuntimeException("term does not have a type");
    }

    @Override
    public LTerm reduce(Memory m) {
        if(term.isReducible())
            return new Cast(term.reduce(m), t);
        if(term.type(new Environment()).isSubtypeOf(t))
            return term.clone();
        throw new RuntimeException("Cast Error: " + term.type(new Environment()) + " is not a Subtype of " + t);
    }


    @Override
    public LTerm replaceFreeVar(String varName, LTerm insertedTerm) {
        return new Cast(term.replaceFreeVar(varName, insertedTerm), t);
    }

}
