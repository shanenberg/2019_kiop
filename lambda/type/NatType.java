package lambda.type;

public class NatType extends Type {
	
	public Type clone() {
		return new NatType();
	}

	@Override
	public boolean isSubtypeOf(Type left) {
		return (left instanceof NatType); // Reflexivit�t!
	}

}