package lambda.type;

abstract public class Type {

	/*
	 * Types override equal, because they need to me compared and each type is a different instance.
	 * I.e. identify comparison does not make sense, so check for equality is needed to make sure that
	 *   Nat = Nat or (Bool->Nat)->Nat = (Bool->Nat)->Nat
	 */
	public boolean equals(Object o) {
		return o.getClass() == this.getClass();
	}
	
	public abstract Type clone();

	public boolean isSubtypeOf(Type left) {
		return this.getClass().equals(left.getClass());
	}

}
