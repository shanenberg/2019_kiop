package lambda.type;

public class SumType extends Type {

	public Type leftType;
	public Type rightType;
	
	public SumType(Type leftType, Type rightType) {
		super();
		this.leftType = leftType;
		this.rightType = rightType;
	}

	@Override
	public boolean isSubtypeOf(Type t) {
		if(t instanceof SumType) {
			SumType t_ = (SumType) t;
			return (leftType.equals(t_.leftType) && rightType.equals(t_.rightType));
		}
		return false;
	}

	@Override
	public SumType clone() {
		return new SumType(leftType.clone(), rightType.clone());
	}
}
