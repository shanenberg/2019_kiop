package lambda.type;

public class RefType extends Type {

	public final Type elementType;
	
	public RefType(Type elementType) {
		super();
		this.elementType = elementType;
	}

	@Override
	public Type clone() {
		return new RefType(elementType.clone());
	}

}
