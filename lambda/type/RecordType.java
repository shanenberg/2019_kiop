package lambda.type;

import java.util.HashMap;
import java.util.Map;

public class RecordType extends Type {
	public final Map<String, Type> fieldTypes;
	
	public RecordType(Map<String, Type> fieldTypes) {
		super();
		this.fieldTypes = fieldTypes;
	}

	@Override
	public Type clone() {
		Map<String, Type> newFields = new HashMap<String, Type>(fieldTypes);
		return new RecordType(newFields);
	}

    @Override
    public boolean isSubtypeOf(Type t) {
        if(t instanceof RecordType) {
            RecordType t_ = (RecordType) t;
            for(Map.Entry<String, Type> fieldType : t_.fieldTypes.entrySet()) {
                if(!(fieldTypes.containsKey(fieldType.getKey()) && fieldTypes.get(fieldType.getKey()).isSubtypeOf(fieldType.getValue())))
                    return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof RecordType) {
            return ((RecordType)o).fieldTypes.entrySet().equals(this.fieldTypes.entrySet());
        }
        return false;
    }
}
