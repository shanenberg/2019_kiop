package lambda.type;

public class BoolType extends Type {

	@Override
	public Type clone() {
		return new BoolType();
	}

	@Override
	public boolean isSubtypeOf(Type left) {
		return (left instanceof BoolType); // Reflexivit�t!
	}
}
