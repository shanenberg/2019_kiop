package lambda.type;

/*
 * T->T
 */
public class FunctionType extends Type {
	public Type left;
	public Type right;
	
	public FunctionType(Type left, Type right) {
		super();
		this.left = left;
		this.right = right;
	}


	@Override
	public boolean equals(Object obj) {
		if(! (obj instanceof FunctionType)) return false;
		FunctionType ft = (FunctionType) obj;
		return this.left.equals(ft.left) && this.right.equals(ft.right);
	}

	@Override
	public boolean isSubtypeOf(Type t) {
		if(t instanceof FunctionType) {
			FunctionType t_ = (FunctionType)t;
			return (right.isSubtypeOf(t_.right) && t_.left.isSubtypeOf(left));
		}
		return false;
	}


	@Override
	public Type clone() {
		return new FunctionType(left.clone(), right.clone());
	}
}
